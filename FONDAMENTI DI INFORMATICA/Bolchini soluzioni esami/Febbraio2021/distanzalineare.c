#include <stdio.h>
#include <stdlib.h>
float *distanza_lineare(float start, float end, int numero, int escludistop){
	float *ris;
	float step;
	int i;
	
	if(numero==0){
		return NULL;    /*in questi due casi return NULL subito*/
	}
	if(end==start){         
		return NULL;
	}
	
	if(ris=malloc(sizeof(float)*numero)){     /*devo restituire una struttura dati, quindi devo creare un array dinamico per comtenere i valori*/
		if(escludistop==0){
			step=(end-start)/(numero-1);
		}else{                                /*il valore di step (la variazione della parte decimale) cambia in base al valore di escludistop*/
			step=(end -start)/numero;
		}
		ris[0]=start;             /*nella prima cella va sempre messo start, vedi testo esame*/
		for(i=1; i<numero; i++){
			ris[i]=ris[i-1]+step;   /*nelle altre celle inserisco il valore precedente, aumentato di step (vedi testo esame)*/
		}
	}else{
		printf("errore allocazione");   /*ricorda l'else*/
	}
	return ris;
}


















