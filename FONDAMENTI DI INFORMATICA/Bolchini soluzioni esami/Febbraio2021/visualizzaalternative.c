#include <stdio.h>
#include <string.h>
#define LEN 30
int compare(char str1[], char str2[]){
	int i, len1, len2;                               /*sottoprogramma che date due stringhe eseguono ci� che � richiesto(creato per organizzare meglio il sottoprogrammma principale del FILE)*/
	
	len1=strlen(str1);
	len2=strlen(str2);
	if(len1!=len2){     /*lunghezza diversa ---> return 0, non va bene*/
		return 0;
	}
	if(str1[0]==str2[0]){     /*il primo carattere non pu� essere uguale, se cos� fosse --->return 0*/
		return 0;
	}
	for(i=1; i<len1; i++){            /*dopo il primo carattere le stringhe devono essere identiche, altrimenti return 0*/
		if(str1[i]!=str2[i]){
			return 0;
		}
	}
	return 1;   /*se il sottoprogramma arriva ad eseguire questa riga--> return 1, le stringhe rispettano i parametri*/
}
int visualizza_alternative(FILE *fp, char cerca[]){   /*ATTENZIONE, il sottoprogramma riceve un riferimento ad un file di testo, NON il suo nome*/
	char parola[LEN+1];
	int cont=0;
	                                               /*il file � gi� aperto... ma in ogni caso non saprei il suo nome (perch� non lo ricevo)*/
	while(fscanf(fp, "%s", parola)!=EOF){
		if(compare(cerca, parola)){     /*utilizzo il sottoprogramma prima sviluppato e stampo ci� che � richiesto dal problema*/
		printf("%s ", parola);
		cont++;
	}
}
return cont;
}
