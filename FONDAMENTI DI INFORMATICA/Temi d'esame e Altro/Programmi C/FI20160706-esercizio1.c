/*
Si consideri una lista dinamica di interi definita come:

struct nodo
{
    int val;
    struct nodo *next;
};

typedef struct nodo *lista;

Implementare la seguente funzione:

void ruota(lista *l);

che riceve in ingresso la testa di una lista l di interi, la modifica spostando l’ultimo elemento della lista l
in testa. Ad esempio, la lista 2->5->7->9 viene modificata in 9->2->5->7.

Note. Nel caso la lista l sia vuota, la sua testa contiene il valore NULL. Se necessario, è possibile implementare ulteriori funzioni di supporto
da utilizzare all’interno della funzione ruota.

*/

#include <stdio.h>


void ruota(lista *l) {
    lista pre;
    lista cur;

    if(*l!=NULL && (*l)->next!=NULL) { //la condizione è vera quando la lista non è vuota e non contiene un solo elemento
        pre = *l;
        cur = (*l)->next;

        while(cur->next != NULL) { //faccio scorrere la lista finchè non arrivo alla fine
            pre = cur; //tengo traccia dell'elemento precedente della lista
            cur = cur->next; //e di quello corrente
        }

        pre->next = NULL; //faccio diventare il precedente l'ultimo elemento della lista
        cur->next = *l; //inserisco l'ultimo elemento dela lista come primo
        *l = cur; // aggiorno la lista originale

    }
}
