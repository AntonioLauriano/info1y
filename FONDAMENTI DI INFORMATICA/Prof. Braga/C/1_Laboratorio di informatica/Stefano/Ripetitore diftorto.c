#include <stdio.h>

int main()
{
	char c=0, spaz=1, voc=0;
	printf("Inserisci una frase: \n");
	scanf("%c", &c);
	
	while(c!='\n'){
		
		if(c!=' '){
			spaz=1;
		}
		if(c=='a' || c=='e' || c=='i' || c=='o' || c=='u'){
			voc=1;
		}
		if(c==' '){
			spaz=1;
		}
		if(c=='r' || c=='R'){
			c='V';
		}
		if(c=='s' || c=='S'){
			c='F';		
		}
		if((c=='c' || c=='C') && spaz==1){
			c='H';
		}
		if(c<='z' && c>='a'){
			c=c-('a'-'A');
		}
		if(voc==0){
			printf("%c", c);
		}
		else{
			printf("%cF%c", c, c);
			voc=0;
			
		}
		scanf("%c", &c);
		
	}
	
	return 0;
}
