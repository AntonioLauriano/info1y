#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX 1000000

int main(){
	
	amicabili(MAX);
	
	return 0;
}

void amicabili( int max ){
	int first, second, t, sum;
	for( first=1 ; first<=max ; first++ ){
		t=(first/2)+1;
		for( second=0 ; t>0 ; t-- ){
			if( first%t==0 ){
				second+=t;
			}
		}
		t=(second/2)+1;
		for ( sum=0 ; t>0 ; t--){
			if( second%t==0 ){
				sum+=t;
			}
		}
		if( first==sum && first>second ){
			printf("Amicabili: %6d <--> %6d\n", first, second);
		}
	}
}
