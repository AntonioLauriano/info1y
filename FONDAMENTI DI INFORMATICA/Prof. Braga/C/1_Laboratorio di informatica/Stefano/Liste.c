#include <stdio.h>

typedef int TipoElemento;

typedef struct EL {
  TipoElemento info;
  struct EL * prox;
} ElemLista;

int main(){
	int i;
	ElemLista * lista;
	ElemLista * start2;
	ElemLista * temp;
	lista= (ElemLista *) malloc(sizeof(ElemLista));
	lista->info=0;
	start2=lista;
	for(i=1; i<=6; i++){
		temp= (ElemLista *) malloc(sizeof(ElemLista));
		temp->info=i;
		temp->prox=NULL;
		start2->prox=temp;
		start2=temp;
	}
	printf("%d\n", Dimensione(lista));
	visualizzalista(lista);

	return 0;
}

void visualizzalista(ElemLista *lista){
	if(!listavuota(lista)){
		printf("%d", lista->info);
		if(lista->prox!=NULL){
			printf("-->");
		}
		else{
			printf("--|");
		}
		visualizza(lista->prox);
	}
	else{
		printf("--|");
	}
}

int listavuota(ElemLista * lista){
	if(lista==NULL){
		return 1;
	}
	else{
		return 0;
	}
}
