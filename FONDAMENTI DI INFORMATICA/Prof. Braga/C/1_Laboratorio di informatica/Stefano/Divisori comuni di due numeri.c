#include <stdio.h>

int main(){
	
	int n=0;
	int m=0;
	int k=0;
	int t=1;
	int e=1;
	printf("---------DIVISORI COMUNI DI DUE NUMERI (POSITIVI)---------\n");
	printf("Scrivi due numeri: ");
	scanf("%d%d", &n, &m);
	if(n<=m)
		k=n;
	else
		k=m;
	while(k>1){
		if(n%k!=0)
			k--;
		else{
			if(m%k!=0)
				k--;
			else{
					if(e==1){
						printf("I divisori comuni sono: ");
						e=2;
					}
				printf("%d ", k);
				k--;
				t=2;
			}			
		}
			
	}
	if(t!=2)
		printf("I numeri %d e %d sono primi tra loro", n, m);
	else
		printf("\nI numeri %d e %d non sono primi tra loro", n, m);
	
	return 0;
	
}
