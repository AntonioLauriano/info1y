#include <stdio.h>
#include <string.h>
#include <math.h>

int main(){
	
	int n, b;
	n=1024;
	b=3;
	printf("%d", is_power(n, b));
	
	return 0;
}

int is_power (int n, int b){
	if(n==1){
		return 1;
	}
	if(n==0){
		return 0;
	}
	return (b!= 0 && n%b==0 && (b==1 || is_power(n/b, b)));
}
