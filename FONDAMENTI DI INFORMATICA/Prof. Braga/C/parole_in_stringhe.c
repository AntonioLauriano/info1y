#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define max 200

int check_par (char * par1, char * par2)
{

	if(strlen(par1) == strlen(par2))
	{
		for(int i = 0; i <= strlen(par1); i++)
		{
			if (par1[i] != par2[i])
			{
				return 0;
			}
			
		}
		
		return 1;
	}
	else
	{
		return 0;	
	}
	
}

int pif (char * frase, char * par)
{
	
	int j = 0, i = 0;
	char temp [max];
	
	do
	{
	
		for(i = j; (frase[i] >= 'a' && frase[i] <= 'z'); i++)
		{
			temp[i-j] = frase[i];
			
		}
		
		temp[i] = '\0';
		
		//printf("TEMP: %s\nPAR: %s\ni: %d\nj: %d\n",temp,par,i,j);
		
		if (check_par(par,temp) == 1)
		{
			return 1;	
		}
		else
		{
			j=i+1;
		}
	
	}while(frase[j-1] != '\0');
	
	return 0;
	
}

int main(void)
{

	char s1 [max];
	
	char p1 [max];
	char p2 [max];
	
	printf("Insert string: ");
	fgets(s1,max,stdin);
	
	printf("Insert to search in string: ");
	scanf("%s", p1);
	/*printf("Insert second par: ");
	scanf("%s", p2);
	
	printf("%d\n",check_par(p1,p2));*/
	
	printf("%d\n", pif(s1,p1));
	
}