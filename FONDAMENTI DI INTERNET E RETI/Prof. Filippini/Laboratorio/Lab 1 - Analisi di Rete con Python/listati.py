# http://www.planetb.ca/syntax-highlight-word

import requests

r = requests.get('http://www.google.com')
print r.elapsed.microseconds / 1000, 'ms'

###############################################################################
import requests

for _ in range(10):
    r = requests.get('http://www.google.com')
    print r.elapsed.microseconds / 1000, 'ms'

###############################################################################
import requests

tempi = []
for _ in range(10):
    r = requests.get('http://www.google.com')
    tempi.append(r.elapsed.microseconds / 1000)

print 'min', min(tempi)
print 'avg', sum(tempi)/len(tempi)
print 'max', max(tempi)

###############################################################################
import requests
import matplotlib.pyplot as plt

tempi = []
for _ in range(10):
    r = requests.get('http://www.google.com')
    tempi.append(r.elapsed.microseconds / 1000)

print 'min', min(tempi)
print 'avg', sum(tempi)/len(tempi)
print 'max', max(tempi)

plt.figure()
plt.plot(tempi)
plt.ylim([0, max(tempi)])
plt.show()

###############################################################################
import requests
import matplotlib.pyplot as plt

tempi = []
for _ in range(10):
    r = requests.get('http://www.google.com')
    tempi.append(r.elapsed.microseconds / 1000)

print 'min', min(tempi)
print 'avg', sum(tempi)/len(tempi)
print 'max', max(tempi)

plt.figure()
plt.plot(tempi)
plt.ylim([0, 1.1*max(tempi)])
plt.xlabel('ID richiesta')
plt.ylabel('[ms]')
plt.title('Test http://www.google.com')
plt.grid()
plt.show()

###############################################################################
import requests

siti = ['http://www.gazzetta.it', 'http://www.netflix.com', 'http://www.facebook.com']
for url in siti:
    print 'Test', url
    tempi = []
    for _ in range(10):
        r = requests.get(url)
        tempi.append(r.elapsed.microseconds/1000)
    print 'min', min(tempi)
    print 'avg', sum(tempi)/len(tempi)
    print 'max', max(tempi)

###############################################################################
import requests
import matplotlib.pyplot as plt

m = 0 # massimo tra i massimi
plt.figure()
siti = ['http://www.gazzetta.it', 'http://www.netflix.com', 'http://www.facebook.com']
for url in siti:
    print 'Test', url
    tempi = []
    for _ in range(10):
        r = requests.get(url)
        tempi.append(r.elapsed.microseconds/1000)
    plt.plot(tempi, label=url)
    print 'min', min(tempi)
    print 'avg', sum(tempi)/len(tempi)
    print 'max', max(tempi)
    m = max([m, max(tempi)])  # ricalcolo il massimo tra i massimi

plt.ylim([0, 1.1*m])
plt.xlabel('ID richiesta')
plt.ylabel('[ms]')
plt.title('Test tempi di risposta')
plt.legend(loc='lower right', fontsize=8)
plt.grid()
plt.show()
###############################################################################
import requests

tempi1 = []
for _ in range(5):
    r = requests.get('http://www.google.com')
    tempi1.append(r.elapsed.microseconds/1000)
avg1 = sum(tempi1)/len(tempi1)

tempi2 = []
for _ in range(5):
    r = requests.get('http://www.youtube.com')
    tempi2.append(r.elapsed.microseconds/1000)
avg2 = sum(tempi2)/len(tempi2)

if avg1 < avg2:
    print 'http://www.google.com'
else:
    print 'http://www.youtube.com'
###############################################################################
import requests

siti = ['http://www.google.com', 'http://www.youtube.com', 'http://www.polimi.it',
        'http://www.wikipedia.org', 'http://www.amazon.com', 'http://www.twitter.com']
avg = []
for url in siti:
    print 'Test', url
    tempi = []
    for _ in range(10):
        r = requests.get(url)
        tempi.append(r.elapsed.microseconds/1000)
    print 'avg', sum(tempi)/len(tempi)
    avg.append(sum(tempi)/len(tempi))

print siti[avg.index(min(avg))], min(avg)
###############################################################################
import socket
from geoip import geolite2

# Ottiene l'indirizzo IP a partire dall'hostname
hostname = 'www.gazzetta.it'
addr = socket.gethostbyname(hostname)
print "L'indirizzo IP di", hostname, "e'", addr

# Ottiene lo stato a partire dall'indirizzo IP
match = geolite2.lookup(addr)
if match is not None:
	print hostname, 'si trova in ', match.country, match.continent, match.timezone

###############################################################################
import socket
from geoip import geolite2
from geopy.geocoders import Nominatim

geoloactor = Nominatim()

siti = ['gazzetta.it', 'netflix.com', 'facbook.com', 'google.com', 'polimi.it']
for url in siti:
    addr = socket.gethostbyname(url)
    match = geolite2.lookup(addr)
    if match is not None:
        print "L'indirizzo IP di", url, "e'", addr
        print "Il fuso orario e'", match.timezone
        print "Le coordiante GPS sono", match.location
        print "L'indirizzo fisico e'", geoloactor.reverse(match.location)
        print
